plugins {
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.serialization") version ("1.9.0")
    id("com.github.johnrengelman.shadow") version ("8.1.1")
    application
}

group = "com.gitlab.candicey.waybarcolour"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.1")
}

tasks.test {
    useJUnitPlatform()
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "com.gitlab.candicey.waybarcolour.MainKt"
    }
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass = "com.gitlab.candicey.waybarcolour.MainKt"
}