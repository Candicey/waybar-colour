package com.gitlab.candicey.waybarcolour

import java.io.File

val map = mutableMapOf<String, Pair<String, ((RGBA, (String) -> RGBA?) -> RGBA)?>>(
    "text" to ("color1" to { it, getColour -> if (isLightTheme) it.darken(10).alpha((0.8 * 255).toInt()) else getColour("color15")!! }),
    "background" to ("color11" to { it, getColour -> if (isLightTheme) RGBA(255, 255, 255, (0.875 * 255).toInt()) else it.darken(10) }),
    "border" to ("color6" to { it, getColour -> it.alpha((0.85 * 255).toInt()) }),
    "workspace_button_text" to ("color3" to { it, getColour -> it.alpha((0.8 * 255).toInt()) }),
    "workspace_button_active_text" to ("color1" to { it, getColour -> if (isLightTheme) it.darken(10).alpha((0.8 * 255).toInt()) else getColour("color6")!!.lighten(40) }),
    "workspace_button_active_background" to ("color6" to { it, getColour -> it.alpha((0.4 * 255).toInt()) }),
    "workspace_button_hover_text" to ("color1" to { it, getColour -> if (isLightTheme) it.darken(30).alpha((0.8 * 255).toInt()) else getColour("color6")!!.lighten(25) }),
    "workspace_button_hover_background" to ("color4" to { it, getColour -> it.darken(10).alpha((0.5 * 255).toInt()) }),
)

var isLightTheme = true

fun main(args: Array<String>) {
    val waybarConfig = args.getOrNull(0) ?: "$HOME/.config/waybar/style.css"
    val colourConfig = args.getOrNull(1) ?: "$HOME/.cache/wal/colors.css"
    val lightOrDark = args.getOrNull(2) ?: "dark"

    if (lightOrDark == "dark") {
        isLightTheme = false
        switchToDark()
    }

    val waybarConfigFile = File(waybarConfig)
    val colourFile = File(colourConfig)

    val parsedWaybarConfig = waybarConfigFile
        .readText()
        .lines()
        .mapNotNull {
            it.takeIf { it.startsWith("@define-color") }
                ?.split(" ", limit = 3)
                ?.let { (_, name, colour) -> name to (parseColour(colour.removeSuffix(";")) ?: return@mapNotNull null) }
        }
        .toMap()

    val parsedColourConfig = colourFile
        .readText()
        .lines()
        .map(String::trim)
        .mapNotNull {
            it.takeIf { it.startsWith("--") }
                ?.split(":")
                ?.let { (name, colour) -> name.substring(2) to (parseColour(colour.removeSuffix(";").trim()) ?: return@mapNotNull null ) }

        }
        .toMap()

    fetchCustomOptions(parsedColourConfig)

    parsedWaybarConfig
        .mapNotNull { (name, _) ->
            map[name]?.let { (newName, transform) ->
                val newColour = parsedColourConfig[newName] ?: error("Could not find colour $newName")
                name to (transform?.invoke(newColour, parsedColourConfig::get) ?: newColour)
            }
        }
        .toMap()
        .let {
            waybarConfigFile.writeText(
                waybarConfigFile
                    .readText()
                    .lines()
                    .joinToString("\n") { line ->
                        if (line.startsWith("@define-color")) {
                            val name = line.split(" ", limit = 3)[1]
                            val colour = it[name] ?: return@joinToString line
                            "@define-color $name rgba(${colour.red}, ${colour.green}, ${colour.blue}, ${colour.alpha / 255.0});"
                        } else {
                            line
                        }
                    }
            )
        }
}

private fun fetchCustomOptions(parsedColourConfig: Map<String, RGBA>) {
    val file = File("$HOME/scripts/daemon/theming/custom-waybar-options.json")
    if (!file.exists()) {
        return
    }

    val json = file.readText()
    val parsed = JSON.decodeFromString<CustomOptions>(json)

    val option = parsed[System.getenv("THEME_IMAGE") ?: return] ?: return
    for ((property, colour) in option) {
        val set = map[property]
        val before = set?.first ?: continue
        val init = colour.value ?: before
        val initialReferentialColour by lazy {
            set.second?.invoke(parsedColourConfig[init]!!, parsedColourConfig::get) ?: parsedColourConfig[init]!!
        }
        map[property] = init to when (colour.type) {
            ColourOptionType.STATIC -> { _, _ -> colour.staticColour!! }
            ColourOptionType.DYNAMIC -> { it, _ -> RGBA.fromHex(runProgramme(colour.value!!, it.toHex()).trim()) }
            ColourOptionType.REFERENTIAL -> { _, _ -> colour.referentialOption?.let { (red, green, blue, modifyRed, modifyGreen, modifyBlue, alpha, darken, lighten) -> initialReferentialColour.darken(darken).lighten(lighten).modifyRed(modifyRed).modifyGreen(modifyGreen).modifyBlue(modifyBlue).let { red?.let { c -> it.red(c) } ?: it }.let { green?.let { c -> it.green(c) } ?: it }.let { blue?.let { c -> it.blue(c) } ?: it }.alpha(alpha) } ?: initialReferentialColour }
        }
    }
}

private fun switchToDark() {
    for ((property, option) in map) {
        val color = option.first

        map[property] = when (color) {
            "foreground" -> "background"

            "background" -> "foreground"

            else -> {
                val stringBuilder = StringBuilder()
                for (i in color.length - 1 downTo 0) {
                    val int = color[i].digitToIntOrNull() ?: break
                    stringBuilder.insert(0, int)
                }

                val int = stringBuilder.toString().toInt()
                val text = color.substring(0, color.length - stringBuilder.length)

                // the int runs from either 1-8 or 9-15
                // if it is 1-8 and the number is 2, mirror it, meaning 2 becomes 7
                // the same goes for 3 and 6, 4 and 5
                // if it is 9-15 and the number is 10, mirror it, meaning 10 becomes 15
                // the same goes for 11 and 14, 12 and 13
                val newInt = when (int) {
                    in 1..8 -> 9 - int
                    in 9..15 -> 24 - int
                    else -> error("Invalid number $int")
                }

                "$text$newInt"
            }
        } to option.second
    }
}