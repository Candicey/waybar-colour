package com.gitlab.candicey.waybarcolour

fun runProgramme(executable: String, vararg args: String): String {
    val process = ProcessBuilder(executable, *args)
        .redirectOutput(ProcessBuilder.Redirect.PIPE)
        .redirectError(ProcessBuilder.Redirect.PIPE)
        .start()

    val output = process.inputStream.bufferedReader().readText()
    val error = process.errorStream.bufferedReader().readText()

    if (error.isNotBlank()) {
        println("Error running $executable: $error")
    }

    return output
}