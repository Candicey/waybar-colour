package com.gitlab.candicey.waybarcolour

import kotlinx.serialization.Serializable

fun parseColour(string: String): RGBA? =
    when {
        string.startsWith("rgba(") && string.endsWith(")") -> {
            val (red, green, blue, alpha) = string
                .removePrefix("rgba(")
                .removeSuffix(")")
                .split(",")
                .map { it.trim() }
                .mapIndexed { index, value ->
                    when (index) {
                        0, 1, 2 -> value.toInt()
                        3 -> (value.toFloat() * 255).toInt()
                        else -> error("Invalid index $index")
                    }
                }
            RGBA(red, green, blue, alpha)
        }

        string.startsWith("rgb(") && string.endsWith(")") -> {
            val (red, green, blue) = string
                .removePrefix("rgb(")
                .removeSuffix(")")
                .split(",")
                .map { it.trim().toInt() }
            RGBA(red, green, blue, 255)
        }

        string.startsWith("#") -> {
            when (string.length) {
                9 -> RGBA.fromHex(string)
                7 -> RGBA.fromHex(string + "ff")
                else -> null
            }
        }

        else -> null
    }

/**
 * Represents a RGBA colour
 *
 * @property red The red component of the colour (0-255)
 * @property green The green component of the colour (0-255)
 * @property blue The blue component of the colour (0-255)
 * @property alpha The alpha component of the colour (0-255)
 */
@Serializable
data class RGBA(val red: Int, val green: Int, val blue: Int, val alpha: Int = 255) {
    fun darken(amount: Int): RGBA =
        copy(
            red = (red - amount).coerceAtLeast(0),
            green = (green - amount).coerceAtLeast(0),
            blue = (blue - amount).coerceAtLeast(0)
        )

    fun lighten(amount: Int): RGBA =
        copy(
            red = (red + amount).coerceAtMost(255),
            green = (green + amount).coerceAtMost(255),
            blue = (blue + amount).coerceAtMost(255)
        )

    fun red(red: Int): RGBA =
        copy(red = red)

    fun green(green: Int): RGBA =
        copy(green = green)

    fun blue(blue: Int): RGBA =
        copy(blue = blue)

    fun modifyRed(red: Int): RGBA =
        copy(red = this.red + red)

    fun modifyGreen(green: Int): RGBA =
        copy(green = this.green + green)

    fun modifyBlue(blue: Int): RGBA =
        copy(blue = this.blue + blue)

    fun alpha(alpha: Int): RGBA =
        copy(alpha = alpha)

    fun isLight(threshold: Int = 186): Boolean = (red * 0.299 + green * 0.587 + blue * 0.114) > threshold

    fun toHex(): String = "#%02x%02x%02x%02x".format(red, green, blue, alpha)

    companion object {
        fun fromHex(hex: String): RGBA =
            with(hex.replace("#", "")) {
                RGBA(
                    substring(0, 2).toInt(16),
                    substring(2, 4).toInt(16),
                    substring(4, 6).toInt(16),
                    substring(6, 8).toInt(16)
                )
            }
    }
}