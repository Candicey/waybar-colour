package com.gitlab.candicey.waybarcolour

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Map of Image Name to CustomOptions
typealias CustomOptions = Map<String, CustomOption>
// Map of CustomOption Name to ColourOption
typealias CustomOption = Map<String, ColourOption>
@Serializable
data class ColourOption(
    val type: ColourOptionType = ColourOptionType.REFERENTIAL,
    val value: String? = null,
    val staticColour: RGBA? = null,
    val referentialOption: ColourOptionType.ReferentialColourOption? = null,
)

@Serializable
enum class ColourOptionType {
    @SerialName("static")
    STATIC,
    @SerialName("dynamic")
    DYNAMIC,
    @SerialName("referential")
    REFERENTIAL;

    @Serializable
    data class ReferentialColourOption(
        val red: Int? = null,
        val green: Int? = null,
        val blue: Int? = null,
        val modifyRed: Int = 0,
        val modifyGreen: Int = 0,
        val modifyBlue: Int = 0,
        val alpha: Int = 255,
        val darken: Int = 0,
        val lighten: Int = 0,
    )
}