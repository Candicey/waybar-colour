package com.gitlab.candicey.waybarcolour

import kotlinx.serialization.json.Json

val HOME = System.getProperty("user.home")

val JSON = Json {
    ignoreUnknownKeys = true
    prettyPrint = true
}